/*
    SPDX-FileCopyrightText: 2018 Daniel Vrátil <dvratil@kde.org>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef AKREMOTELOG_H_
#define AKREMOTELOG_H_

void akInitRemoteLog();

#endif
